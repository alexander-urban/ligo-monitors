Advanced LIGO Monitors (`ligo-monitors`)
========================================

This repository contains wrappers to run daily monitoring scripts on Advanced
LIGO data, to be attached to the summary pages. It is based on the following
pure-python packages:

* [GWDetChar](https://anaconda.org/conda-forge/gwdetchar)
* [Hveto](https://anaconda.org/conda-forge/hveto)

These are installed on the `detchar` shared-user accounts on LDAS clusters at
CIT, LHO, and LLO, under conda environments called `ligo-summary-3.9`:

```bash
$ conda activate /home/detchar/.conda/envs/ligo-summary-3.9
```
